[![pipeline status](https://gitlab.com/iamjohnnymgg/dotfiles/badges/main/pipeline.svg)](https://gitlab.com/iamjohnnymgg/dotfiles/-/commits/main)
[![coverage report](https://gitlab.com/iamjohnnymgg/dotfiles/badges/main/coverage.svg)](https://gitlab.com/iamjohnnymgg/dotfiles/-/commits/main)
# MacOS dotfiles

Battlestation dotfiles for MacOS.  Setup and configure user config files for various applications.

## Installs

- brew
- npm
- pyenv
- rbenv
- vscode