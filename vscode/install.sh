#!/usr/bin/env bash

export DOTFILE_PATH="${HOME}/.dotfiles"

#shellcheck disable=SC2002
cat "${DOTFILE_PATH}"/vscode/extensions.list | xargs -L 1 code --install-extension
