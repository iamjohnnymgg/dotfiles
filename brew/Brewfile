tap "chef/chef"
tap "heroku/brew"
tap "homebrew/bundle"
tap "homebrew/cask"
tap "homebrew/cask-fonts"
tap "homebrew/core"
tap "homebrew/services"
tap "mas-cli/tap"
tap "wata727/tflint"
tap "snyk/tap"

# Install mas-cli, Mac Apple Store CLI to manage our installed apps
# https://github.com/mas-cli/mas

brew "mas"

# Install apple store apps

mas "LastPass", id: 926036361
mas "Magnet", id: 441258766
mas "Skitch", id: 425955336
mas "SnippetsLab", id: 1006087419
mas "Xcode", id: 497799835

# Base Libs

brew "automake"
brew "zlib"

# Base Requirements

## shell completion scripts

brew "bash-completion"

## Networking

brew "nmap"
brew "socat"
brew "telnet"
# cask "viscosity"

## System

brew "htop"
brew "jq"
brew "tree"
brew "wget"

# Environment Apps

brew "direnv"
brew "elvish"
brew "pyenv"
brew "rbenv"
brew "jenv"
brew "tfenv"
brew "pre-commit"

# Encryption

cask "aws-vault"
brew "gnupg"
brew "lastpass-cli"
cask "1password"
cask "1password-cli"
cask "keybase"
brew "monkeysphere"
brew "pass"
brew "wifi-password"

# Virtualization

brew "docker"
brew "docker-machine-driver-xhyve"
brew "kubernetes-cli"
cask "vagrant"
cask "vagrant-manager"
cask "virtualbox"
cask "virtualbox-extension-pack"

# Programming

brew "node"
brew "sbt"

# Databases

brew "postgresql"
brew "sqlite"

# Development Related

brew "bench"
brew "hadolint"
brew "packer"
brew "tflint"
brew "m-cli"
brew "ghq"
cask "visual-studio-code"
brew "terraform-docs"

# Cloud Drives

cask "google-backup-and-sync"

# Unsorted

cask "alfred"
cask "chromedriver"
brew "dep"
brew "gawk"
brew "geckodriver"
brew "graphviz"
cask "gimp"
cask "postman"
brew 'vim'
brew "shellcheck"
brew "snyk"
